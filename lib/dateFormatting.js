//GLOBAL FUNCTIONS

function currentDateString (){
  return dateFormat(new Date(), "dddd, mmmm d, yyyy, h:MM:ss TT");
}

function todayDateOnlyString(){ //return just today's date, without time. Currently this must match the format above
  return dateFormat(new Date(), "dddd, mmmm d, yyyy");
}

function timeFromDate(date){
  return dateFormat(date, "h:MM:ss TT");
}

function getStartOfYesterday(){
	var yesterday = new Date();
    yesterday.setDate(yesterday.getDate()-1);
    yesterday.setHours(0);
    yesterday.setMinutes(0);
    yesterday.setSeconds(0);
    yesterday.setMilliseconds(0); 
    return yesterday;
}

function getEndOfYesterday(){
	var yesterday = new Date();
    yesterday.setDate(yesterday.getDate()-1);
    yesterday.setHours(0);
    yesterday.setMinutes(0);
    yesterday.setSeconds(0);
    yesterday.setMilliseconds(0); 
    return yesterday;
}