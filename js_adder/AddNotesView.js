var AddNotesView = Parse.View.extend({
  el: ".content",
  logOut: function(e) {
      Parse.User.logOut();
      new LogInView();
      this.undelegateEvents();
      delete this;
    },
    saveNote: function(){
      var noteText = $("#enter-note").val();
      if(noteText.length<1){
        alert("No note entered.");
        return;
      }
      var self = this;
      
      console.log("Adding new note: "+noteText);

      var noteObj = new Note();
      var date = new Date($("#year").val(),$("#month").val(), $("#day").val(), 0,0,0,0);     

      noteObj.set("noteText",noteText);
      noteObj.set("siteDate", date);
      noteObj.set("user",Parse.User.current());
      noteObj.set("ACL", new Parse.ACL(Parse.User.current()));
      var relation = noteObj.relation("tags");
      var checkedTags = this.tagManager.getCheckedTags();

      for (var i = 0; i < checkedTags.length; i++){
      	relation.add(checkedTags[i]);
      }

      noteObj.save(null,{
        success:function(){
          console.log("Note object saved successfully");
          self.notes.add(noteObj);
          $("#enter-note").val("");
        },
        error:function(e){
          console.log("Error: note object failed to save.");
        }
      });
    },
    events:{
      "click .log-out": "logOut",
      "click #save-note": "saveNote"
    },

    initialize:function(){
      this.$el.html(_.template($("#note-view-template").html()));
      _.bindAll(this, 'addOne', 'addAll', 'render', 'logOut');

      this.notes = new NoteList();
      

      this.notes.query = new Parse.Query(Note);
      this.notes.query.equalTo("user", Parse.User.current());
     //query for notes made today

      this.notes.bind('add',     this.addOne);
      this.notes.bind('reset',   this.addAll);
      this.notes.bind('all',     this.render);

      // Fetch all the items for this user
      this.notes.fetch({
        success: function(collection){
          console.log('Success getting collection of notes ' + collection.size());
        },
        error: function(collectiom,error){
          console.log('Error getting collection of notes');
        }
      });

      var date = new Date();
      $("#month").val(date.getMonth());
      $("#day").val(date.getDate());
      $("#year").val(date.getFullYear());


      this.tagManager = new TagManager();
      this.$("#tags").append(this.tagManager.render().el);
    },
    addOne: function(note) {
      console.log("addOne from AddNotesView.js")
      var view = new NoteView({model: note});
      this.$("#note-list").append(view.render().el);
     



    },

    // Add all items in the Todos collection at once.
    addAll: function(collection, filter) {
      this.$("#note-list").html("");
      this.notes.each(this.addOne);
    },
});