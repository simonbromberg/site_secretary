//Note item

var NoteView  = Parse.View.extend({
 // class: "li",
  template: _.template($('#note-item-template').html()),
  events:{
    //"click .delete-note" : "clear"
  },

  initialize: function(){
     _.bindAll(this, 'render', 'remove', 'addAll', 'addOne');
      this.model.bind('change', this.render);
      this.model.bind('destroy', this.remove);
      // var self = this;
     

      
      // this.tags = new TagList();
      // this.tags.query = new Parse.Query(Tag);
      // this.tags.query.equalTo("user", Parse.User.current());

      // this.tags.bind('add',     this.addOne);
      // this.tags.bind('reset',   this.addAll);
      // this.tags.bind('all',     this.render);

      // this.tags.fetch({
      //   success: function(collection){
      //     console.log('Success getting collection of tags ' + collection.size());
      //   },
      //   error: function(collectiom,error){
      //     console.log('Error getting collection of tags');
      //   }
      // });

// var relation = this.model.relation("tags");
      // relation.query().find({
      //   success: function(list){
      //     console.log("Got relations: "+list.length);
      //     self.tags.reset(list);
      //   },
      //   error: function(error){
      //     console.log('Error getting relations');
      //   }
      // });
    },

     render: function(){
         $(this.el).html(this.template(this.model.toJSON()));
          var self = this;

      var relation = this.model.relation("tags");
            relation.query().find({
              success: function(list){
                console.log("Got relations: "+list.length);
                for(var i = 0; i <list.length; i++){
                  self.$("#memo-tags-list").append("<li>#"+list[i].get("tagText")+"</li>");
                }
              },
              error: function(error){
                console.log('Error getting relations');
              }
            });
      return this;
     },
    // Remove the item, destroy the model.
    clear: function() {
      this.model.destroy();
    },

    addOne: function(tag) {
      console.log("NoteView.js: tagText: " + tag.get("tagText"));
      var view = new MemoTagView({model: tag});
      this.$("#memo-tags-list").append(view.render().el);
    },

    addAll: function(collection, filter) {
      console.log("At add all in NoteView.js");
      this.$("#memo-tags-list").html("");
      this.tags.each(this.addOne);
     
    },
});