var TagManager = Parse.View.extend({
 	id:"tags",
    // Cache the template function for a single item.
    template: _.template($('#tag-list-template').html()),
  events:{
      "click #new-tag-submit": "newTag",
      "keypress #new-tag-text"      : "updateOnEnter"
    },
    updateOnEnter: function(e){
    	if(e.keyCode == 13){
    		this.newTag();
    	}
    },
  newTag: function(){
  	var self = this;
    var text = $("#new-tag-text").val();
     if(text.length < 1){
        alert("Please enter a valid tag");
        return;
      }
      if(this.tagExistsAlready(text)){
      	console.log("Tag exists already");
      	alert("Tag already exists");
      	return;
      }
    var newTag = new Tag({
    	tagText: text,
        user:    Parse.User.current(),
        ACL:     new Parse.ACL(Parse.User.current())
    });
    newTag.save(null,{
        success: function(){
          self.tags.add(newTag);
        }
      });
    $("#new-tag-text").val("");
  },
  tagExistsAlready: function(text){
  	  	var retVal = false;
  	  	var N = this.tags.size;
  	  	var i;
  	  	var tag;
  	  	for (i = 0; i < N; i++){
  	  		tag = this.tags.at(i);
  	  		if (tag.get("tagText") === text){
  	  			return true;
  	  		}
  	  	}
  	  	return retVal;
  },
  initialize: function(){
  	console.log("Initialized TagManager");
  	this.$el.html(_.template($("#tag-list-template").html()));
  	_.bindAll(this, 'addOne', 'addAll', 'render');
       // Create our collection of workers
      this.tags = new TagList();
      // Setup the query for the collection to look for workers from the current user
      this.tags.query = new Parse.Query(Tag);
      this.tags.query.equalTo("user", Parse.User.current());
      this.tags.query.ascending("tagText");
      this.tags.bind('add',     this.addOne);
      this.tags.bind('reset',   this.addAll);
      this.tags.bind('all',     this.render);

      this.tagsOnView = new Array();
       // Fetch all the items for this user
      this.tags.fetch({
        success: function(collection){
          console.log('Success getting collection of tags');
        },
        error: function(collectiom,error){
          console.log('Error getting collection of tags');
        }
      });
  },
    render: function(){
   //   $(this.el).html(this.template());
      return this;
    },
  addOne: function(tag) {
      var view = new TagView({model: tag});
      this.$("#tag-list").append(view.render().el);
      this.tagsOnView.push(view);
    },

    // Add all items in the collection at once.
    addAll: function(collection, filter) {
      this.$("#tag-list").html("");
      this.tags.each(this.addOne);
    },
    getCheckedTags: function(){
    	var checkedTags = new Array();
    	for(var i = 0; i<this.tagsOnView.length;i++){
    		if(this.tagsOnView[i].isChecked){
    			checkedTags.push(this.tagsOnView[i].model);
    		}
    	}
    	return checkedTags;
    }
});

