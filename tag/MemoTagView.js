var MemoTagView = Parse.View.extend({
	tagName: "li",
	template: _.template($('#memo-tag-template').html()),
  initialize: function(){
    console.log("MemoTagView.js initialize function");
     _.bindAll(this, 'render', 'remove');
      this.model.bind('change', this.render);
      this.model.bind('destroy', this.remove);
    },
    render: function(){
      console.log("MemoTagView.js render function " + this.model.get("tagText"));
      $(this.el).html(this.template(this.model.toJSON()));
      return this;
    },
    // Remove the item, destroy the model.
    clear: function() {
      this.model.destroy();
    }
});