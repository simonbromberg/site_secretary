//Tag item

var TagView  = Parse.View.extend({
  tagName: "li",
  template: _.template($('#tag-item-template').html()),
   events: {
          "click .toggle": "toggleTag"
    },
    toggleTag: function(){
      console.log("tag toggled ");
      this.isChecked = !this.isChecked;
    },
  initialize: function(){
     _.bindAll(this, 'render', 'remove');
      this.model.bind('change', this.render);
      this.model.bind('destroy', this.remove);
      this.isChecked = true;
    },
    render: function(){
      $(this.el).html(this.template(this.model.toJSON()));
     this.checkBox = this.$(".toggle");
      return this;
    },
    // Remove the item, destroy the model.
    clear: function() {
      this.model.destroy();
    }
});